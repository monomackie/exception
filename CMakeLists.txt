cmake_minimum_required(VERSION 3.20)
project(exception)

set(CMAKE_CXX_STANDARD 17)

add_library(exception STATIC Exception.h Exception.cpp)
